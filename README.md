# godot-stargames

* Godot 3.5
* Shooter espacial 2D

## Descripción

Experimento de creación de un juego:
* Ambientado en Starwars.
* Mecánicas Arcade Retro.
* Aspecto 2D pixel art.

## Ideas

* Scramble game: (https://twitter.com/retrochenta/status/1621860863467552768?t=SwSkdw8ouBiVMQg4YDASpw&s=35) Este mes se cumplen 42 años de la salida al mercado del mítico videojuego Scramble, uno de mis favoritos de las salas recreativas. La de partidas que habremos echado!

## Ideas Pixel Art

Ships
* Naves: https://super-ficcion.com/2023/01/las-naves-del-universo-de-star-wars-nave/
* Main ship: https://foozlecc.itch.io/void-main-ship

Planets
* Tatooine: https://duckduckgo.com/?q=tatooine+pixel+art&t=fpas&iar=images&iax=images&ia=images&iai=https%3A%2F%2Fi.pinimg.com%2F736x%2Fea%2Fb5%2F35%2Feab5358d374114417474ba99823c3d31--happy-star-wars-day-star-wars-.jpg
* Tatooine: https://duckduckgo.com/?q=tatooine+pixel+art&t=fpas&iar=images&iax=images&ia=images

Rocks
* Free crystals: https://craftpix.net/freebies/free-crystals-pixel-art-asset-pack/
* Rocks: https://craftpix.net/freebies/free-rocks-pixel-art-asset-pack/
* https://www.freepik.com/free-vector/flat-design-animation-frames-element-collection_32449119.htm#query=2d%20game%20sprites&position=1&from_view=keyword

Characters:
* https://hajileee.itch.io/hajileees-fantasy-characters-pack
* https://samuellee.itch.io/spartan-warrior-animated-pixel-art


## Colaboraciones

**Modo 1**
* Clonar el repositorio, crear rama devel y hacer los cambios
```
git clone https://gitlab.com/dvarrui/godot-starfit.git
cd godot-starfit
git branch devel
git checkout devel
```

