extends Node2D

export var num_stars = 60
onready var layer1 = $parallax/layer1

var Star = preload("res://world/background/star.tscn")
var size = Vector2(1024, 600)

func _ready():
	create_stars(30, layer1)

func create_stars(num, layer):
	for _index in range(0, num):
		var star = Star.instance()
		star.speed = 0
		star.position.x = int(floor(rand_range(0, size.x)))
		star.position.y = int(floor(rand_range(0, size.y)))
		layer.add_child(star)
