extends Node2D

enum State {
	ALIVE,
	DEAD
}

onready var sprite = $sprite
export var speed = 30
export var layer = ""
export var mode = Vector2(0,4)
var state = State.ALIVE
var bg = null

func _ready():
	bg = get_parent().get_parent()
	reset()

func reset():
	state = State.ALIVE
	var frame = int(floor(rand_range(mode.x, mode.y)))
	sprite.frame = frame
	
func _physics_process(delta):
	if state == State.ALIVE:
		position.y += delta * speed
		if position.y > 600:
			out_of_screen()

func out_of_screen():
	state = State.DEAD
	position.y = 0
	if bg.maximum_exceeded_into_layer(self.layer):
		queue_free()
	else:
		reset()
