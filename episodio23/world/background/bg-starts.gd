extends Node2D

export var num_stars = 30
onready var timer_add = $timers/add
onready var timer_delete = $timers/delete

var Star = preload("res://world/background/star.tscn")
var size = Vector2(1024, 600)
var layers = []

func _ready():
	layers = [ 0, $layer1, $layer2, $layer3 ]
	# timer_add.start()
	# timer_delete.start()
	create_stars(num_stars)
	
func create_stars(num):
	create_stars_into_layer(num, 1, 8, Vector2(0,2))
	create_stars_into_layer(num, 2, 16, Vector2(3,4))
	create_stars_into_layer(num, 3, 32, Vector2(4,8))

func create_stars_into_layer(num, layer_index, speed, mode):
	for _index in range(0, num):
		var star = Star.instance()
		star.position.x = int(floor(rand_range(0, size.x)))
		star.position.y = int(floor(rand_range(0, size.y)))
		star.speed = speed
		star.mode = mode
		star.layer = layer_index
		layers[layer_index].add_child(star)
	
func add_star_into_layer(index):
	var star = Star.instance()
	star.position.x = int(floor(rand_range(0, size.x)))
	star.position.y = 0
	layers[index].add_child(star)

func maximum_exceeded_into_layer(index):
	if layers[index].get_child_count() > num_stars:
		return true
	else:
		return false

func _on_timeradd_timeout():
	add_star_into_layer(0)

func _on_delete_timeout():
	num_stars -= 1
