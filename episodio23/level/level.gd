extends Node2D

enum State {
	PLAY
	ENDGAME
}

var state = State.PLAY
onready var music = $music
onready var timer_startgame = $timer/startgame
onready var label = $label
onready var timer_endgame = $timer/endgame

func _ready():
	Loader.build_file_into_level("data.txt", self)
	timer_startgame.start(2)
	music.play(0)

func _process(_delta):
	if Input.is_action_pressed("exit_game"):
		get_tree().quit()

func finish_game():
	state = State.ENDGAME
	label.visible = true
	label.text = "GAME OVER"
	timer_endgame.start(3)

func _on_startgame_timeout():
	label.visible = false
	timer_startgame.stop()

func _on_endgame_timeout():
	get_tree().quit()
